# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikimedia Deployment Scheduler
#
# Wikimedia Deployment Scheduler is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Wikimedia Deployment Scheduler is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Wikimedia Deployment Scheduler. If not, see <http://www.gnu.org/licenses/>.
import logging

import flask
import flask_wtf
from wtforms.fields import IntegerField
from wtforms.fields import SelectField
from wtforms.fields import StringField
from wtforms.fields import SubmitField
from wtforms.validators import DataRequired

from . import mediawiki
from . import settings

COOKIE_IRC_NICK = "irc_nick"
COOKIE_NAME = "name"

logger = logging.getLogger(__name__)


def get_nick_from_cookie():
    return flask.request.cookies.get(COOKIE_IRC_NICK)


def get_name_from_cookie():
    name = flask.request.cookies.get(COOKIE_NAME)
    if not name:
        return get_nick_from_cookie()
    return name


def get_backport_windows():
    if settings.MEDIAWIKI_CONSUMER_TOKEN == "DUMMY_VALUE_FOR_TESTS":
        # HACK: deterministic response for CI tests
        return [
            (
                "2024-06-11 13:00 SF",
                "Tuesday, June 11 UTC late backport window",
                {"data-epoch": 1718136000000},
            ),
            (
                "2024-06-12 00:00 SF",
                "Wednesday, June 12 UTC morning backport window",
                {"data-epoch": 1718175600000},
            ),
            (
                "2024-06-12 06:00 SF",
                "Wednesday, June 12 UTC afternoon backport window",
                {"data-epoch": 1718197200000},
            ),
            (
                "2024-06-12 13:00 SF",
                "Wednesday, June 12 UTC late backport window",
                {"data-epoch": 1718222400000},
            ),
            (
                "2024-06-13 00:00 SF",
                "Thursday, June 13 UTC morning backport window",
                {"data-epoch": 1718262000000},
            ),
        ]
    mw = mediawiki.Client.default_client()
    windows = mw.get_upcoming_windows()
    # (value, label, render_kw{})
    return [
        (
            window[0],
            window[1],
            {
                "data-epoch": int(window[2].timestamp() * 1000),
            },
        )
        for window in windows
    ]


class GerritIdForm(flask_wtf.FlaskForm):
    gerrit_id = IntegerField(
        label="Gerrit change number",
        validators=[DataRequired()],
        render_kw={"placeholder": "Gerrit change number"},
    )
    submit = SubmitField("Schedule")


class BackportForm(flask_wtf.FlaskForm):
    irc_nick = StringField(
        label="Your IRC nick",
        validators=[DataRequired()],
        default=get_nick_from_cookie,
    )
    name = StringField(
        label="Your display name",
        default=get_name_from_cookie,
        render_kw={"placeholder": "(Defaults to IRC nick)"},
    )
    description = StringField(
        label="Description",
    )
    window = SelectField(
        label="Backport window",
        choices=get_backport_windows,
    )
    submit = SubmitField("Schedule deployment")
