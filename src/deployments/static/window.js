/*!
 * Add local dates to backport windows.
 * Copyright (c) 2024 Wikimedia Foundation and contributors.
 * License: GPL-3.0-or-later
 */
(() => {
	'use strict'
	const $h2 = document.querySelector( 'h2' );
	const dt = new Date( +$h2.getAttribute( 'data-epoch' ) );
	const $when = document.createElement( 'h3' );
	$when.append(
		dt.toLocaleString(
			// sv-SE locale uses ISO 8601 compatible formats
			'sv-SE',
			{ dateStyle: 'short', timeStyle: 'short' }
		)
	);
	$when.className = 'card-subtitle mb-2';
	$h2.after( $when );
	const tz = Intl.DateTimeFormat().resolvedOptions().timeZone;
	const $tzNote = document.createElement( 'small' );
	$tzNote.textContent = `Dates and times are shown in your time zone (${ tz }).`;
	const $wrapper = document.createElement( 'div' );
	$wrapper.append( $tzNote );
	$when.after( $wrapper );
})()
