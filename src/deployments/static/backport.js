/*!
 * Add local dates to backport windows.
 * Copyright (c) 2024 Wikimedia Foundation and contributors.
 * License: GPL-3.0-or-later
 */
(() => {
	'use strict'
	const $select = document.querySelector( '#window' );
	const $options = Array.from( $select.options );
	$options.forEach( ( opt ) => {
		const dt = new Date( +opt.getAttribute( 'data-epoch' ) );
		opt.append(
			' (',
			dt.toLocaleString(
				// sv-SE locale uses ISO 8601 compatible formats
				'sv-SE',
				{ dateStyle: 'short', timeStyle: 'short' }
			),
			')'
		);
	} );
	const $selectRow = $select.closest( '.row' );
	const tz = Intl.DateTimeFormat().resolvedOptions().timeZone;
	const $tzNote = document.createElement( 'small' );
	$tzNote.className = 'col-lg-10 offset-2';
	$tzNote.textContent = `Dates and times are shown in your time zone (${ tz }).`;
	$selectRow.append( $tzNote );
})()
