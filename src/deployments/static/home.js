/*!
 * Add local dates to backport windows.
 * Copyright (c) 2025 Wikimedia Foundation and contributors.
 * License: GPL-3.0-or-later
 */
(() => {
	'use strict'
	const $upcoming = document.querySelector( '#upcoming' );
	const $windows = $upcoming.querySelectorAll( 'a' );
	$windows.forEach( ( window ) => {
		const dt = new Date( +window.getAttribute( 'data-epoch' ) );
		window.append(
			' (',
			dt.toLocaleString(
				// sv-SE locale uses ISO 8601 compatible formats
				'sv-SE',
				{ dateStyle: 'short', timeStyle: 'short' }
			),
			')'
		);
	} );
	const $header = $upcoming.querySelector( '.card-header' );
	const tz = Intl.DateTimeFormat().resolvedOptions().timeZone;
	const $tzNoteDiv = document.createElement( 'div' );
	$tzNoteDiv.className = 'text-center';
	const $tzNote = document.createElement( 'small' );
	$tzNote.textContent = `Dates and times are shown in your time zone (${ tz }).`;
	$tzNoteDiv.append( $tzNote );
	$header.append( $tzNoteDiv );
})()
