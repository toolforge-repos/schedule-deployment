# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikimedia Deployment Scheduler
#
# Wikimedia Deployment Scheduler is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Wikimedia Deployment Scheduler is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Wikimedia Deployment Scheduler. If not, see <http://www.gnu.org/licenses/>.
import datetime
import itertools
import logging
import re

import mwclient
import mwparserfromhell

from . import settings
from . import utils

logger = logging.getLogger(__name__)

EVENT_TEMPLATE = "Deployment calendar event card"
IRCNICK_TEMPLATE = "ircnick"
BACKPORT_MARKER = "[[Backport windows"
INSTRUCTIONS_NICK = "irc-nickname"
RE_STRIP_WRAPPER = re.compile(
    r"^\s*<div [^>]+>(?P<payload>.*)</div>\s*$",
    re.DOTALL,
)


def parse_wikitext(wikitext):
    return mwparserfromhell.parse(wikitext)


def extract_when(template):
    return str(template.get("when").value).strip()


def extract_window_name(template):
    window = parse_wikitext(template.get("window").value)
    link = window.filter_wikilinks()[0]
    return str(link.text)


def extract_argument_str(template, arg_name, default=None):
    if template.has(arg_name):
        arg = template.get(arg_name)
        return str(arg.value)
    return str(default)


def extract_user(template):
    nick = extract_argument_str(template, 1, "unknown").strip()
    name = extract_argument_str(template, 2, nick).strip()
    return {
        "name": name,
        "nick": nick,
    }


def extract_deployers(template):
    who = parse_wikitext(template.get("who").value)
    deployers = []
    for tmpl in who.ifilter_templates():
        if tmpl.name.matches(IRCNICK_TEMPLATE):
            deployers.append(extract_user(tmpl))
    return deployers


def extract_changes(client, template):
    what = parse_wikitext(template.get("what").value)
    changes = []
    ircnick_idxs = [
        what.index(nick) for nick in get_templates_by_name(what, "ircnick")
    ]
    for pair in itertools.pairwise(ircnick_idxs):
        ircnick = what.nodes[pair[0]]
        items = what.nodes[pair[0] + 2 : pair[1]]
        html = client.parse_to_html(
            client.deploy_page,
            "".join(str(item) for item in items),
        )
        changes.append([extract_user(ircnick), html])
    return changes


def find_all_backport_windows(wikitext):
    """Extract a list of backport windows from a blob of wikitext."""
    windows = []
    wikicode = parse_wikitext(wikitext)
    for template in wikicode.ifilter_templates():
        if template.name.matches(EVENT_TEMPLATE):
            if template.has("window") and template.get(
                "window",
            ).value.startswith(BACKPORT_MARKER):
                when_str = extract_when(template)
                when = utils.window_when_to_datetime(when_str)
                name = when.strftime("%A, %B %d ")
                name += extract_window_name(template)
                windows.append((when_str, name, when))
    return windows


def find_window_template(parsed, when):
    for template in parsed.ifilter_templates():
        if template.name.matches(EVENT_TEMPLATE):
            if extract_when(template) == when:
                return template
    return None


def parse_window_template(client, tmpl):
    when = extract_when(tmpl)
    return {
        "name": extract_window_name(tmpl),
        "when": {
            "str": when,
            "dt": utils.window_when_to_datetime(when),
        },
        "length": int(str(tmpl.get("length").value).strip()),
        "deployers": extract_deployers(tmpl),
        "changes": extract_changes(client, tmpl),
    }


def get_templates_by_name(node, name):
    return [
        tmpl
        for tmpl in node.ifilter_templates(recursive=False)
        if tmpl.name.matches(name)
    ]


def add_item_to_window(window, nick, name, item):
    logger.debug("Before: %s", window)
    new_ircnick = utils.build_ircnick(nick, name) + "\n"
    if not item.endswith("\n"):
        item += "\n"
    search_nick = nick.lower()
    what = window.get("what").value
    insert_next = False
    edit_made = False
    for section in get_templates_by_name(what, "ircnick"):
        section_nick = section.params[0].lower()
        logger.debug("section: %s; nick: %s", section, section_nick)
        if insert_next or section_nick == INSTRUCTIONS_NICK:
            if not insert_next:
                # Create a new section
                logger.info("Adding ircnick template for %s", nick)
                what.insert_before(section, new_ircnick, recursive=False)
            what.insert_before(section, item, recursive=False)
            edit_made = True
            break
        if section_nick == search_nick:
            # Found existing section for our user
            # We now want to insert before the next item.
            insert_next = True
    if not edit_made:
        # We walked off the end of the section list without making an edit.
        # This happens when someone has ovewritten or deleted the placeholder.
        logger.warning(
            "%s not found. Appending our content to window.",
            INSTRUCTIONS_NICK,
        )
        if not insert_next:
            # Add a new section to the end of the window.
            logger.info("Adding ircnick template for %s", nick)
            what.append(new_ircnick)
        what.append(item)
    logger.debug("After: %s", window)


class Client:

    _default_instance = None

    @classmethod
    def default_client(cls):
        """Get a MediaWiki client using the default credentials."""
        if cls._default_instance is None:
            logger.debug("Creating default instance")
            cls._default_instance = cls(
                settings.MEDIAWIKI_HOST,
                settings.MEDIAWIKI_CONSUMER_TOKEN,
                settings.MEDIAWIKI_CONSUMER_SECRET,
                settings.MEDIAWIKI_ACCESS_TOKEN,
                settings.MEDIAWIKI_ACCESS_SECRET,
                settings.MEDIAWIKI_DEPLOYMENTS_PAGE,
            )
        return cls._default_instance

    def __init__(
        self,
        host,
        consumer_token,
        consumer_secret,
        access_token,
        access_secret,
        deploy_page,
    ):
        self.mwsite = mwclient.Site(
            host,
            consumer_token=consumer_token,
            consumer_secret=consumer_secret,
            access_token=access_token,
            access_secret=access_secret,
            clients_useragent=utils.USER_AGENT,
        )
        self.deploy_page = deploy_page

    def get_upcoming_windows(self):
        """Get list of upcoming windows."""
        page = self.mwsite.Pages[self.deploy_page]
        now = datetime.datetime.now(datetime.UTC)
        # Include windows that are in progress.
        # Assume all windows last for one hour.
        an_hour_ago = now - datetime.timedelta(hours=1)
        windows = find_all_backport_windows(page.text())
        return [window for window in windows if window[2] >= an_hour_ago]

    def get_window_data(self, epoch):
        page = self.mwsite.Pages[self.deploy_page]

        old_wikitext = page.text()
        parsed = parse_wikitext(old_wikitext)
        when = utils.epoch_to_window_when(epoch, "SF")
        tmpl = find_window_template(parsed, when)
        if tmpl is None:
            # T388556: Try again in CET timezone
            when = utils.epoch_to_window_when(epoch, "CET")
            tmpl = find_window_template(parsed, when)
        try:
            return parse_window_template(self, tmpl)
        except AttributeError:
            logger.exception("Failed to get data for window %s", epoch)
            return None

    def update_window(self, form, change):
        when = form.window.data
        nick = form.irc_nick.data
        name = form.name.data
        item = utils.build_backport_item(form, change)
        _, window_label, _, _ = utils.get_selected(form.window)
        summary = f"Add [[gerrit:{change['_number']}]] to {window_label}"

        page = self.mwsite.Pages[self.deploy_page]

        old_wikitext = page.text()
        parsed = parse_wikitext(old_wikitext)
        window = find_window_template(parsed, when)
        add_item_to_window(window, nick, name, item)
        new_wikitext = str(parsed)
        if new_wikitext == old_wikitext:
            raise RuntimeError("No changes made to wikitext")

        # T374735: don't mark edits as bot edits for watchlisters
        resp = page.edit(new_wikitext, summary=summary, bot=False)
        logger.debug("page.edit: %s", resp)
        if resp.get("nochange"):
            raise RuntimeError("Edit was a no-op.")
        return resp

    def parse_to_html(self, title, text):
        resp = self.mwsite.post(
            "parse",
            text=text,
            title=title,
            prop="text",
            disablelimitreport="1",
            utf8="1",
            formatversion="2",
        )
        html = resp["parse"]["text"]
        m = RE_STRIP_WRAPPER.match(html)
        return m.group("payload")
