# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikimedia Deployment Scheduler
#
# Wikimedia Deployment Scheduler is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Wikimedia Deployment Scheduler is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Wikimedia Deployment Scheduler. If not, see <http://www.gnu.org/licenses/>.
import datetime
import json
import logging
import secrets

import flask
import flask_bootstrap
import flask_wtf
import gunicorn.app.wsgiapp
import markupsafe

from . import forms
from . import gerrit
from . import mediawiki
from . import settings
from . import utils
from .logging import GunicornLogger
from .version import __version__

logger = logging.getLogger(__name__)

app = flask.Flask(__name__)
app.config.from_prefixed_env(prefix="WEB")
app.config.update(
    SESSION_COOKIE_HTTPONLY=True,
    SESSION_COOKIE_SAMESITE="Strict",
    SESSION_REFRESH_EACH_REQUEST=False,
    BOOTSTRAP_SERVE_LOCAL=True,
)
if not app.secret_key:
    # Can be configured durably with a WEB_SECRET_KEY envvar
    app.secret_key = secrets.token_urlsafe(48)

bootstrap = flask_bootstrap.Bootstrap5(app)
csrf = flask_wtf.csrf.CSRFProtect(app)


@app.context_processor
def inject_version():
    return {"version": __version__}


@app.context_processor
def inject_now():
    return {"now": datetime.datetime.now(datetime.UTC)}


@app.get("/")
def home():
    mw = mediawiki.Client.default_client()
    ctx = {
        "form": forms.GerritIdForm(),
        "windows": mw.get_upcoming_windows(),
    }
    gerrit_id = flask.request.args.get("gerrit_id")
    if gerrit_id:
        ctx["form"].gerrit_id.default = gerrit_id
        ctx["form"].process()
    return flask.render_template("home.html", **ctx)


@app.post("/")
def home_post():
    form = forms.GerritIdForm()
    if form.validate_on_submit():
        return flask.redirect(
            flask.url_for("backport", gerrit_id=form.gerrit_id.data),
        )
    return flask.redirect(flask.url_for("home"))


@app.get("/healthz")
def healthz():
    """Trivial 'is this process alive' check."""
    return {"status": "OK"}, 200


# https://pynative.com/python-serialize-datetime-into-json/
class DateTimeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, (datetime.date, datetime.datetime)):
            return obj.isoformat()


@app.template_filter("ppjson")
def ppjson(value, indent=2):
    return json.dumps(value, indent=indent, cls=DateTimeEncoder)


@app.route("/window/<int:epoch>", methods=["GET", "POST"])
def window(epoch):
    mw = mediawiki.Client.default_client()
    window = mw.get_window_data(epoch)
    if window is None:
        # FIXME: return a cute 404 message
        flask.abort(404)

    # TODO: assert <6 patches scheduled
    form = forms.GerritIdForm()
    if form.validate_on_submit():
        # Return 303 -> /backport/<gerrit_id>?epoch=<epoch>
        return flask.redirect(
            flask.url_for(
                "backport",
                gerrit_id=form.gerrit_id.data,
                epoch=epoch,
            ),
            code=303,
        )

    ctx = {
        "window": window,
        "form": form,
    }
    return flask.render_template("window.html", **ctx)


@app.route("/backport/<int:gerrit_id>", methods=["GET", "POST"])
def backport(gerrit_id):
    gerrit_client = gerrit.RESTClient.default_client()
    try:
        change = gerrit_client.get_change(gerrit_id)
    except gerrit.APIError as ex:
        logger.exception("Failed to fetch change %s", gerrit_id)
        flask.flash(ex.result, "error")
        return flask.redirect(flask.url_for("home"))

    valid = utils.validate_backport(change)
    if not valid.ok:
        flask.flash(valid.message, "error")
        return flask.redirect(flask.url_for("home", gerrit_id=gerrit_id))

    form = forms.BackportForm()
    epoch = flask.request.args.get("epoch", None)
    if epoch is not None:
        epoch = int(epoch)
        window_id = utils.window_id_for_epoch(epoch)
        logger.info("Epoch %s -> window '%s'", epoch, window_id)
        form.window.default = window_id

    if form.validate_on_submit():
        mw = mediawiki.Client.default_client()
        resp = mw.update_window(form, change)
        if settings.FEATURE_GERRIT_COMMENT:
            comment = utils.build_backport_gerrit_comment(form)
            gerrit_client.post_change_comment(gerrit_id, comment)

        _, window_label, _, _ = utils.get_selected(form.window)
        wiki_url = f"https://{settings.MEDIAWIKI_HOST}/wiki"
        revision = resp["newrevid"]
        msg = markupsafe.Markup(
            f"Change {gerrit_id} added to {window_label} "
            f'(<a href="{wiki_url}/Special:Diff/{revision}">diff</a>)',
        )
        flask.flash(msg, "success")
        epoch = utils.epoch_for_window_id(form.window.data)
        resp = flask.redirect(flask.url_for("window", epoch=epoch))
        resp.set_cookie(
            forms.COOKIE_IRC_NICK,
            form.irc_nick.data,
            max_age=2764800,  # 32 days
        )
        resp.set_cookie(
            forms.COOKIE_NAME,
            form.name.data,
            max_age=2764800,  # 32 days
        )
        return resp

    form.description.default = change["subject"]
    form.process()

    ctx = {
        "change": change,
        "form": form,
        "gerrit_id": gerrit_id,
    }
    return flask.render_template("backport.html", **ctx)


class Application(gunicorn.app.base.BaseApplication):
    def __init__(self, application, options=None):
        self.options = options
        self.application = application
        super().__init__()

    def load_config(self):
        clean = {
            key: value
            for key, value in self.options.items()
            if key in self.cfg.settings and value is not None
        }
        for key, value in clean.items():
            self.cfg.set(key.lower(), value)

    def load(self):
        return self.application


def main():
    """Multi-purpose web service."""

    def pre_request(worker, req):  # noqa: U100
        pass

    opts = {
        "accesslog": "-",
        "bind": settings.WEB_BIND,
        "logger_class": GunicornLogger,
        "pre_request": staticmethod(pre_request),
        "worker_class": "gthread",
        "threads": settings.WEB_WORKERS,
        "timeout": 300,
    }
    return Application(app, opts).run()
