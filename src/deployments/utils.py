# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikimedia Deployment Scheduler
#
# Wikimedia Deployment Scheduler is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Wikimedia Deployment Scheduler is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Wikimedia Deployment Scheduler. If not, see <http://www.gnu.org/licenses/>.
import dataclasses
import datetime
import logging
import re

import pytz
import requests

from . import mediawiki
from . import settings

USER_AGENT = "{name} ({url}; {email}) python-requests/{vers}".format(
    name="schedule-deployment",
    url="https://wikitech.wikimedia.org/wiki/Tool:Schedule-deployment",
    email=f"{settings.TOOL_NAME}.maintainers@toolforge.org",
    vers=requests.__version__,
)

TIMEZONES = {
    "CET": pytz.timezone("Europe/Berlin"),
    "DE": pytz.timezone("Europe/Berlin"),
    "PDT": pytz.timezone("ETC/GMT-7"),
    "PST": pytz.timezone("ETC/GMT-8"),
    "SF": pytz.timezone("America/Los_Angeles"),
    "UTC": pytz.utc,
    "Z": pytz.utc,
}

RE_WHEN = re.compile(
    r"(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2}) "
    r"(?P<hour>\d{2}):(?P<minute>\d{2}) "
    r"(?P<tz>[A-Z]{1,3})",
)
RE_CURLY_OPEN = re.compile(r"(?<!{)({)(?!{)")
RE_CURLY_CLOSE = re.compile(r"(?<!})(})(?!})")
RE_RESTORE_TEMPLATES = re.compile(
    r"""
    (?P<keep>
        {{                  # `{{`
        (?! (?: \! | {{ ))  # Not followed by `!` or `{{`
        [^{]? [^{]*         # Anything other than `{{`
    )                       # Things to keep
    (?<! }} )               # Not proceeded by `}}`
    {{!}}                   # An escaped pipe (`|`) to restore
    """,
    flags=re.VERBOSE,
)

logger = logging.getLogger(__name__)


def window_when_to_datetime(when):
    """Convert a 'when' string into a UTC datetime."""
    m = RE_WHEN.match(when)
    if m:
        tz = TIMEZONES.get(m.group("tz"), pytz.utc)
        local_dt = tz.localize(
            datetime.datetime(
                year=int(m.group("year")),
                month=int(m.group("month")),
                day=int(m.group("day")),
                hour=int(m.group("hour")),
                minute=int(m.group("minute")),
            ),
        )
        return local_dt.astimezone(pytz.utc)
    return datetime.datetime.fromtimestamp(0, datetime.UTC)


def epoch_to_window_when(epoch, fake_tz):
    # XXX: this is fragile, but maybe the best we have right now?
    dt_utc = datetime.datetime.fromtimestamp(epoch, tz=datetime.UTC)
    dt_sf = dt_utc.astimezone(TIMEZONES[fake_tz])
    # 2024-08-06 01:00 SF
    when = dt_sf.strftime(f"%Y-%m-%d %H:%M {fake_tz}")
    logger.debug("epoch_to_window_when(%s) => %s", epoch, when)
    return when


@dataclasses.dataclass
class Status:
    ok: bool = True
    message: str = None

    def set_error(self, msg):
        self.ok = False
        self.message = msg


def validate_backport(change):
    valid = Status()
    project = change["project"]
    branch = change["branch"]
    status = change["status"]

    if project == "operations/mediawiki-config":
        if branch != "master":
            valid.set_error(
                "MediaWiki config patches must be on the master branch.",
            )
    elif (
        project == "mediawiki/core"
        or project.startswith("mediawiki/extensions/")
        or project.startswith("mediawiki/skins/")
    ):
        if not branch.startswith("wmf/"):
            valid.set_error(
                f"Patches on the {branch} branch of a MediaWiki repo cannot "
                "be backported. Cherry-pick to a `wmf/` release branch "
                "and try again.",
            )
    else:
        valid.set_error("Backport windows are only for MediaWiki repos.")

    if valid.ok and status != "NEW":
        valid.set_error("Only unmerged changes can be backported.")

    if valid.ok and change.get("work_in_progress", False):
        valid.set_error(
            "Remove `Work in Progress` before scheduling backport",
        )

    return valid


def get_change_type(change):
    if change["project"] == "operations/mediawiki-config":
        return "config"
    return change["branch"].split("/")[1]


def get_bugs(change):
    bugs = [
        "{{phabricator|%s}}" % bug["id"]
        for bug in change.get("tracking_ids", [])
        if bug["system"] == "Phab"
    ]
    if bugs:
        return "- " + " ".join(bugs)
    return ""


def template_arg_safe(s):
    """Make the provided string safe for use as an arg to a wikitext
    template.
    """
    # Wrap lone `{` and `}` in `<nowiki>...</nowiki>`
    s = RE_CURLY_OPEN.sub(r"<nowiki>\1</nowiki>", s)
    s = RE_CURLY_CLOSE.sub(r"<nowiki>\1</nowiki>", s)

    # Replace all embedded `|` with the `{{!}}` magic word
    s = s.replace("|", "{{!}}")

    # Unescape `|` if it looks deliberate
    s = RE_RESTORE_TEMPLATES.sub(r"\g<keep>|", s)

    return s


def build_backport_item(form, change):
    gid = change["_number"]
    subject = template_arg_safe(form.description.data or change["subject"])
    ctype = get_change_type(change)
    bugs = get_bugs(change)
    return f"{{{{deploy|type={ctype}|gerrit={gid}|title={subject}|status=}}}} {bugs}"


def build_ircnick(nick, name=None):
    if not name:
        name = nick
    return f"{{{{ircnick|{nick}|{name}}}}}"


def get_selected(select):
    selected = [choice for choice in select.iter_choices() if choice[2]]
    if selected:
        return selected[0]
    else:
        return (None, None, None, None)


def build_backport_gerrit_comment(form):
    when_str, window_label, _, _ = get_selected(form.window)
    utc_when = window_when_to_datetime(when_str)
    window_url = (
        f"https://{settings.MEDIAWIKI_HOST}"
        f"/wiki/{settings.MEDIAWIKI_DEPLOYMENTS_PAGE}"
        f"#deploycal-item-{utc_when.strftime('%Y%m%dT%H%M')}"
    )
    epoch = int(utc_when.timestamp())
    zonestamp_url = f"https://zonestamp.toolforge.org/{epoch}"
    return (
        f"Scheduled for deployment in the [{window_label}]({window_url}) "
        f"at [{utc_when.strftime('%Y-%m-%d %H:%M UTC')}]({zonestamp_url})"
    )


def window_id_for_epoch(epoch):
    mw = mediawiki.Client.default_client()
    windows = mw.get_upcoming_windows()
    for window in windows:
        ts = window[2].timestamp()
        if ts == epoch:
            return window[0]
    return None


def epoch_for_window_id(window_id):
    dt = window_when_to_datetime(window_id)
    return dt.timestamp()
