Wikimedia Deployment Scheduler
==============================

Add a patch to an upcoming backport window on the
https://wikitech.wikimedia.org/wiki/Deployments calendar.

License
-------
Licensed under the [GPL-3.0-or-later][] license. See [COPYING][] for the full
license.

Anchor logo & favicon derived from [OpenMoji-black 2693.svg][] By
[OpenMoji][], [CC BY-SA 4.0][]

[GPL-3.0-or-later]: https://www.gnu.org/licenses/gpl-3.0.en.html
[COPYING]: https://gitlab.wikimedia.org/toolforge-repos/wikibugs2/-/blob/main/COPYING
[OpenMoji-black 2693.svg]: https://commons.wikimedia.org/wiki/File:OpenMoji-black_2693.svg
[OpenMoji]: http://openmoji.org/
[CC BY-SA 4.0]: https://creativecommons.org/licenses/by-sa/4.0
