# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikimedia Deployment Scheduler
#
# Wikimedia Deployment Scheduler is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Wikimedia Deployment Scheduler is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Wikimedia Deployment Scheduler. If not, see <http://www.gnu.org/licenses/>.
import json

import deployments.forms
import deployments.settings
import deployments.utils

from . import root

data_path = root / "tests" / "data" / "utils"


def test_window_when_to_datetime():
    test_cases = {
        "": "1970-01-01T00:00:00+00:00",
        "2024-05-27 00:00 SF": "2024-05-27T07:00:00+00:00",
        "2024-05-27 09:00 DE": "2024-05-27T07:00:00+00:00",
        "2024-05-27 03:00 SF": "2024-05-27T10:00:00+00:00",
        "2024-05-27 12:00 DE": "2024-05-27T10:00:00+00:00",
        "2024-10-28 12:00 CET": "2024-10-28T11:00:00+00:00",
        "2024-10-28 12:00 WTF": "2024-10-28T12:00:00+00:00",
        "2024-11-28 12:00 PDT": "2024-11-28T05:00:00+00:00",
        "2024-11-28 12:00 PST": "2024-11-28T04:00:00+00:00",
        "2024-11-28 12:00 UTC": "2024-11-28T12:00:00+00:00",
        "2024-11-28 12:00 Z": "2024-11-28T12:00:00+00:00",
    }
    for given, expect in test_cases.items():
        dt = deployments.utils.window_when_to_datetime(given)
        assert dt.isoformat() == expect, given


def test_epoch_to_window_when():
    test_cases = {
        (1723446000, "SF"): "2024-08-12 00:00 SF",
        (1723467600, "SF"): "2024-08-12 06:00 SF",
        (1723492800, "SF"): "2024-08-12 13:00 SF",
        (1723532400, "SF"): "2024-08-13 00:00 SF",
        (1723554000, "SF"): "2024-08-13 06:00 SF",
        (1723579200, "SF"): "2024-08-13 13:00 SF",
        (1723618800, "SF"): "2024-08-14 00:00 SF",
        (1723640400, "SF"): "2024-08-14 06:00 SF",
        (1723665600, "SF"): "2024-08-14 13:00 SF",
        (1723705200, "SF"): "2024-08-15 00:00 SF",
        (1723726800, "SF"): "2024-08-15 06:00 SF",
        (1723752000, "SF"): "2024-08-15 13:00 SF",
        (1741852800, "CET"): "2025-03-13 09:00 CET",
    }
    for given, expect in test_cases.items():
        when = deployments.utils.epoch_to_window_when(given[0], given[1])
        assert when == expect, given


def test_validate_backport():
    test_cases = {
        "bad-repo.json": False,
        "mediawiki-bad-branch.json": False,
        "mediawiki-merged.json": False,
        "mediawiki-ok.json": True,
        "skin-bad-branch.json": False,
        "wmf-config-bad-branch.json": False,
        "wmf-config-ok.json": True,
    }

    for fname, expect in test_cases.items():
        with (data_path / fname).open(encoding="utf-8") as f:
            change = json.load(f)
        assert deployments.utils.validate_backport(change).ok == expect, fname


def test_get_change_type():
    ctype = deployments.utils.get_change_type(
        {"project": "operations/mediawiki-config"},
    )
    assert ctype == "config", "operations/mediawiki-config"

    ctype = deployments.utils.get_change_type(
        {"project": "", "branch": "wmf/1.42.0-wmf.16"},
    )
    assert ctype == "1.42.0-wmf.16", "wmf/1.42.0-wmf.16"


def test_get_bugs():
    assert deployments.utils.get_bugs({}) == "", "empty"
    fixture = {"tracking_ids": [{"system": "Phab", "id": "T12345"}]}
    assert deployments.utils.get_bugs(fixture) == "- {{phabricator|T12345}}"


def test_template_arg_safe():
    test_cases = {
        "{{phab:T372750}}": "{{phab:T372750}}",
        "{foo}": "<nowiki>{</nowiki>foo<nowiki>}</nowiki>",
        "foo|bar": "foo{{!}}bar",
        "(de|uk|ja|he|fi)wiki": "(de{{!}}uk{{!}}ja{{!}}he{{!}}fi)wiki",
        "|": "{{!}}",
        "||": "{{!}}{{!}}",
        "{{|}}": "{{{{!}}}}",
        "|{{Hello}}|": "{{!}}{{Hello}}{{!}}",
        "{{phabricator|T372750}}": "{{phabricator|T372750}}",
    }
    for given, expect in test_cases.items():
        when = deployments.utils.template_arg_safe(given)
        assert when == expect, given


def test_build_backport_item(app):
    @app.post("/")
    def index():
        form = deployments.forms.BackportForm()
        with (data_path / "mediawiki-ok.json").open(encoding="utf-8") as f:
            change = json.load(f)
        expect = (
            "{{deploy|type=1.42.0-wmf.16|gerrit=997279"
            "|title=Use decodeURI for comment ID searches as well as heading searches|status=}} "
            "- {{phabricator|T356199}}"
        )
        assert deployments.utils.build_backport_item(form, change) == expect

    app.test_client().post("/", data={"irc_nick": "bd808"})


def test_build_ircnick():
    build_ircnick = deployments.utils.build_ircnick
    assert build_ircnick("bd808") == "{{ircnick|bd808|bd808}}"
    assert (
        build_ircnick("bd808", "BryanDavis") == "{{ircnick|bd808|BryanDavis}}"
    )


def test_build_backport_gerrit_comment(app):
    @app.post("/")
    def index():
        form = deployments.forms.BackportForm()
        expect = (
            "Scheduled for deployment in the "
            "[Wednesday, June 12 UTC late backport window]"
            f"(https://{deployments.settings.MEDIAWIKI_HOST}"
            f"/wiki/{deployments.settings.MEDIAWIKI_DEPLOYMENTS_PAGE}"
            "#deploycal-item-20240612T2000) "
            "at [2024-06-12 20:00 UTC]"
            "(https://zonestamp.toolforge.org/1718222400)"
        )
        assert deployments.utils.build_backport_gerrit_comment(form) == expect

    app.test_client().post(
        "/",
        data={
            "irc_nick": "bd808",
            "name": "bd808",
            "description": "Do the needful",
            "window": "2024-06-12 13:00 SF",
        },
    )
